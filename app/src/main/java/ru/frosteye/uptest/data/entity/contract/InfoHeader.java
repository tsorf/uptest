package ru.frosteye.uptest.data.entity.contract;

import java.util.Date;

/**
 * Created by oleg on 10.02.2018.
 */

public interface InfoHeader {
    Date getStart();
    Date getEnd();
    int getTotal();
    float getAmount();
}
