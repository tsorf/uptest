package ru.frosteye.uptest.data.entity.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import ru.frosteye.ovsa.tool.Stuff;
import ru.frosteye.ovsa.tool.TextTools;
import ru.frosteye.uptest.data.entity.contract.ImageSource;
import ru.frosteye.uptest.data.entity.contract.Product;

/**
 * Created by oleg on 10.02.2018.
 */

public class MockProduct implements Product {

    private ImageSource imageSource = new MockImageSource();
    private CharSequence name;
    private Dynamics dynamics;
    private float amount;

    public ImageSource getImage() {
        return imageSource;
    }

    @Override
    public CharSequence getName() {
        return name;
    }

    @Override
    public Dynamics getDynamics() {
        return dynamics;
    }

    @Override
    public float getAmount() {
        return amount;
    }

    public static List<Product> createMockProducts() {
        List<Product> products = new ArrayList<>();
        Random random = new Random();
        for(int i = 0; i < 40; i++) {
            MockProduct product = new MockProduct();
            product.name = Stuff.randomString(random.nextInt(40) + 10);
            product.amount = random.nextFloat() * 5;
            switch (i % 3) {
                case 0:
                    product.dynamics = Dynamics.UP;
                    break;
                default:
                    product.dynamics = Dynamics.DOWN;
                    break;
            }
            products.add(product);
        }

        return products;
    }
}
