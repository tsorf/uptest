package ru.frosteye.uptest.data.entity.mock;

import ru.frosteye.uptest.R;
import ru.frosteye.uptest.data.entity.contract.ImageSource;

/**
 * Created by oleg on 10.02.2018.
 */

public class MockImageSource implements ImageSource {
    private String url;
    private int resource = R.drawable.img_product_default;

    @Override
    public String getUrl() {
        return url;
    }

    @Override
    public int getResource() {
        return resource;
    }
}
