package ru.frosteye.uptest.data.entity.contract;

/**
 * Created by oleg on 10.02.2018.
 */

public interface ImageSource {
    int getResource();
    String getUrl();
}
