package ru.frosteye.uptest.data.entity.contract;

/**
 * Created by oleg on 10.02.2018.
 */

public interface Product {
    enum Dynamics {
        UP(1),
        DOWN(-1),
        STILL(0);

        int value;

        Dynamics(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    ImageSource getImage();
    CharSequence getName();
    Dynamics getDynamics();
    float getAmount();
}
