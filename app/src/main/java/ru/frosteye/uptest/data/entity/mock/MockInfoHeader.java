package ru.frosteye.uptest.data.entity.mock;

import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import ru.frosteye.uptest.data.entity.contract.InfoHeader;

/**
 * Created by oleg on 10.02.2018.
 */

public class MockInfoHeader implements InfoHeader {

    private Date end = new Date();
    private Date start;
    private float amount;
    private int total;

    public MockInfoHeader() {
        Random random = new Random();
        end = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.WEEK_OF_YEAR, - (random.nextInt(8) + 1));
        start = calendar.getTime();
        total = random.nextInt(10000);
        amount = random.nextFloat() * 6;
    }

    @Override
    public Date getEnd() {
        return end;
    }

    @Override
    public Date getStart() {
        return start;
    }

    @Override
    public float getAmount() {
        return amount;
    }

    @Override
    public int getTotal() {
        return total;
    }
}
