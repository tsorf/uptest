package ru.frosteye.uptest.presentation.view.impl.activity;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import ru.frosteye.ovsa.presentation.presenter.LivePresenter;
import ru.frosteye.ovsa.presentation.view.widget.ListDivider;
import ru.frosteye.uptest.R;
import ru.frosteye.uptest.app.di.component.PresenterComponent;
import ru.frosteye.uptest.data.entity.contract.InfoHeader;
import ru.frosteye.uptest.data.entity.contract.Product;
import ru.frosteye.uptest.presentation.adapter.ProductsAdapter;
import ru.frosteye.uptest.presentation.presenter.contract.MainPresenter;
import ru.frosteye.uptest.presentation.view.contract.MainView;
import ru.frosteye.uptest.presentation.view.impl.widget.InfoHeaderView;

public class MainActivity extends BaseActivity implements MainView {

    @BindView(R.id.activity_main_infoHeader) InfoHeaderView infoHeaderView;
    @BindView(R.id.activity_main_list) RecyclerView list;
    @BindView(R.id.activity_main_swipe) SwipeRefreshLayout swipe;
    @BindView(R.id.activity_main_toolbar) Toolbar toolbar;

    @Inject MainPresenter presenter;

    private ProductsAdapter adapter;
    private List<Product> productList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public void enableControls(boolean enabled, int code) {
        super.enableControls(enabled, code);
        if(enabled) swipe.setRefreshing(false);
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        setSupportActionBar(toolbar);
        setTitle("adagagag");
        adapter = new ProductsAdapter(productList, this);
        list.setLayoutManager(new LinearLayoutManager(this));
        list.addItemDecoration(new ListDivider(this, ListDivider.VERTICAL_LIST));
        list.setAdapter(adapter);
        swipe.setOnRefreshListener(() -> presenter.onLoadData());
    }

    @Override
    protected void attachPresenter() {
        presenter.onAttach(this);
        presenter.onLoadData();
    }

    @Override
    protected LivePresenter<?> getPresenter() {
        return presenter;
    }

    @Override
    protected void inject(PresenterComponent component) {
        component.inject(this);
    }

    @Override
    public void showInfo(InfoHeader header) {
        infoHeaderView.setModel(header);
    }

    @Override
    public void showProducts(List<Product> products) {
        productList.clear();
        productList.addAll(products);
        adapter.notifyDataSetChanged();
    }
}
