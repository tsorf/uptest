package ru.frosteye.uptest.presentation.view.impl.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;

import ru.frosteye.uptest.app.di.component.PresenterComponent;
import ru.frosteye.uptest.app.di.module.PresenterModule;
import ru.frosteye.uptest.app.environment.Uptest;

import butterknife.ButterKnife;
import ru.frosteye.ovsa.presentation.view.activity.PresenterActivity;

/**
 * Created by ovcst on 01.05.2017.
 */

public abstract class BaseActivity extends PresenterActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PresenterComponent component = Uptest.getAppComponent().plus(new PresenterModule(this));
        component.inject(this);
        inject(component);
    }

    protected abstract void inject(PresenterComponent component);

    @Override
    protected void prepareView() {
        ButterKnife.bind(this);
    }
}
