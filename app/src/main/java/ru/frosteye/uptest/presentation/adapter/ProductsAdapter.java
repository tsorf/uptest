package ru.frosteye.uptest.presentation.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import ru.frosteye.uptest.R;
import ru.frosteye.uptest.data.entity.contract.Product;
import ru.frosteye.uptest.presentation.view.impl.widget.ProductView;

/**
 * Created by oleg on 10.02.2018.
 */

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ViewHolder> {

    private List<Product> productList;
    private LayoutInflater inflater;

    public ProductsAdapter(@NonNull List<Product> productList, Context context) {
        this.productList = productList;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(inflater.inflate(R.layout.view_product, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.view.setModel(productList.get(position));
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public final ProductView view;

        public ViewHolder(View itemView) {
            super(itemView);
            this.view = ((ProductView) itemView);
        }
    }
}
