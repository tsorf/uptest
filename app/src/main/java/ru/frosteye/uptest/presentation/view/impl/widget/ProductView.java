package ru.frosteye.uptest.presentation.view.impl.widget;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.AbsoluteSizeSpan;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.frosteye.ovsa.presentation.view.ModelView;
import ru.frosteye.ovsa.presentation.view.widget.BaseConstraintLayout;
import ru.frosteye.ovsa.presentation.view.widget.BaseLinearLayout;
import ru.frosteye.uptest.R;
import ru.frosteye.uptest.data.entity.contract.InfoHeader;
import ru.frosteye.uptest.data.entity.contract.Product;

import static ru.frosteye.ovsa.data.storage.ResourceHelper.getColor;

/**
 * Created by oleg on 10.02.2018.
 */

public class ProductView extends BaseConstraintLayout implements ModelView<Product> {


    @BindView(R.id.view_product_amount) TextView amount;
    @BindView(R.id.view_product_dynamics) ImageView dynamics;
    @BindView(R.id.view_product_image) ImageView image;
    @BindView(R.id.view_product_name) TextView name;

    private Product model;
    private int smallSpanSize;

    public ProductView(Context context) {
        super(context);
    }

    public ProductView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ProductView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void prepareView() {
        if(isInEditMode()) return;
        ButterKnife.bind(this);
        smallSpanSize = getResources().getDimensionPixelSize(R.dimen.smallSanSize);
    }

    @Override
    public Product getModel() {
        return model;
    }

    @Override
    public void setModel(Product model) {
        this.model = model;
        String rawAmount = String.format("%.2f%%", model.getAmount());
        SpannableStringBuilder builder = new SpannableStringBuilder(rawAmount);
        builder.setSpan(new AbsoluteSizeSpan(smallSpanSize),
                rawAmount.length() - 3,
                rawAmount.length() - 1,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        this.amount.setText(builder);
        this.name.setText(model.getName());
        if(model.getImage().getResource() != 0) {
            image.setImageResource(model.getImage().getResource());
        } else {
            image.setImageDrawable(null);
        }
        switch (model.getDynamics()) {
            case UP:
                amount.setTextColor(getColor(R.color.colorDynamicsPositive));
                dynamics.setImageResource(R.drawable.ic_dynamics_up);
                break;
            case DOWN:
                amount.setTextColor(getColor(R.color.colorDynamicsNegative));
                dynamics.setImageResource(R.drawable.ic_dynamics_down);
                break;
            case STILL:
                amount.setTextColor(getColor(R.color.colorDynamicsStill));
                dynamics.setImageResource(R.drawable.ic_dynamics_still);
                break;
        }
    }
}
