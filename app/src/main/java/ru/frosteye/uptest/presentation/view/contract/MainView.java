package ru.frosteye.uptest.presentation.view.contract;

import java.util.List;

import ru.frosteye.ovsa.presentation.view.BasicView;
import ru.frosteye.uptest.data.entity.contract.InfoHeader;
import ru.frosteye.uptest.data.entity.contract.Product;

public interface MainView extends BasicView {
    void showInfo(InfoHeader header);
    void showProducts(List<Product> products);
}
