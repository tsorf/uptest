package ru.frosteye.uptest.presentation.presenter.contract;

import ru.frosteye.uptest.presentation.view.contract.MainView;

import ru.frosteye.ovsa.presentation.presenter.LivePresenter;

public interface MainPresenter extends LivePresenter<MainView> {
    void onLoadData();
}
