package ru.frosteye.uptest.presentation.presenter.impl;

import java.util.List;

import javax.inject.Inject;

import ru.frosteye.uptest.data.entity.contract.Product;
import ru.frosteye.uptest.data.entity.mock.MockInfoHeader;
import ru.frosteye.uptest.data.entity.mock.MockProduct;
import ru.frosteye.uptest.presentation.view.contract.MainView;

import ru.frosteye.ovsa.presentation.presenter.BasePresenter;
import ru.frosteye.uptest.presentation.presenter.contract.MainPresenter;

public class MainPresenterMockImpl extends BasePresenter<MainView> implements MainPresenter {

    @Inject
    public MainPresenterMockImpl() {

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void onLoadData() {
        enableControls(false);
        view.showInfo(new MockInfoHeader());
        List<Product> products = MockProduct.createMockProducts();
        enableControls(true);
        view.showProducts(products);
    }
}
