package ru.frosteye.uptest.presentation.view.impl.widget;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.AbsoluteSizeSpan;
import android.util.AttributeSet;
import android.widget.TextView;

import java.text.SimpleDateFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.frosteye.ovsa.presentation.view.ModelView;
import ru.frosteye.ovsa.presentation.view.widget.BaseConstraintLayout;
import ru.frosteye.ovsa.tool.DateTools;
import ru.frosteye.uptest.R;
import ru.frosteye.uptest.data.entity.contract.InfoHeader;

/**
 * Created by oleg on 10.02.2018.
 */

public class InfoHeaderView extends BaseConstraintLayout implements ModelView<InfoHeader> {

    @BindView(R.id.view_infoHeader_amount) TextView amount;
    @BindView(R.id.view_infoHeader_profit) TextView profit;
    @BindView(R.id.view_infoHeader_total) TextView total;

    private InfoHeader model;
    private SimpleDateFormat formatter;
    private int smallSpanSize;

    public InfoHeaderView(Context context) {
        super(context);
    }

    public InfoHeaderView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public InfoHeaderView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected int getLayoutToInflate() {
        return R.layout.view_info_header;
    }

    @Override
    public InfoHeader getModel() {
        return model;
    }

    @Override
    protected void prepareView() {
        if(isInEditMode()) return;
        ButterKnife.bind(this);
        formatter = new SimpleDateFormat("dd.MM");
        smallSpanSize = getResources().getDimensionPixelSize(R.dimen.smallSanSizeInfo);
    }

    @Override
    public void setModel(InfoHeader model) {
        if(model.getEnd().before(model.getStart())) {
            throw new RuntimeException("End date is before start date: " + model.toString());
        }
        this.model = model;
        total.setText(getResources().getString(R.string.pattern_price, model.getTotal()));
        String rawAmount = String.format("%.5f", model.getAmount());
        SpannableStringBuilder builder = new SpannableStringBuilder(rawAmount);
        builder.setSpan(new AbsoluteSizeSpan(smallSpanSize),
                rawAmount.length() - 5,
                rawAmount.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        amount.setText(builder);
        int daysCount = DateTools.countDaysBetween(model.getStart(), model.getEnd());
        String days = getResources().getQuantityString(R.plurals.days, daysCount);
        profit.setText(getResources().getString(R.string.pattern_info_header,
                formatter.format(model.getEnd()), daysCount, days));
    }
}
