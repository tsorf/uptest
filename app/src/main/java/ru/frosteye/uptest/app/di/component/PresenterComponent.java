package ru.frosteye.uptest.app.di.component;

import ru.frosteye.uptest.app.di.module.PresenterModule;
import ru.frosteye.uptest.app.di.scope.PresenterScope;
import ru.frosteye.uptest.presentation.view.impl.activity.BaseActivity;
import ru.frosteye.uptest.presentation.view.impl.activity.MainActivity;
import ru.frosteye.uptest.presentation.view.impl.fragment.BaseFragment;

import dagger.Subcomponent;

@PresenterScope
@Subcomponent(modules = PresenterModule.class)
public interface PresenterComponent {
    void inject(BaseFragment baseFragment);

    void inject(BaseActivity baseActivity);
    void inject(MainActivity activity);
}
