package ru.frosteye.uptest.app.di.module;

import ru.frosteye.uptest.app.environment.Uptest;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.frosteye.ovsa.di.module.BaseAppModule;

import static ru.frosteye.ovsa.data.storage.ResourceHelper.*;


@Module
public class AppModule extends BaseAppModule<Uptest> {

    public AppModule(Uptest context) {
        super(context);
    }
}
