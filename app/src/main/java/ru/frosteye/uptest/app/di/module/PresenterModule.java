package ru.frosteye.uptest.app.di.module;

import android.view.View;

import ru.frosteye.uptest.app.di.scope.PresenterScope;
import ru.frosteye.uptest.presentation.presenter.contract.MainPresenter;
import ru.frosteye.uptest.presentation.presenter.impl.MainPresenterMockImpl;
import ru.frosteye.uptest.presentation.view.impl.activity.BaseActivity;
import ru.frosteye.uptest.presentation.view.impl.fragment.BaseFragment;

import dagger.Module;
import dagger.Provides;
import ru.frosteye.ovsa.di.module.BasePresenterModule;

@Module
public class PresenterModule extends BasePresenterModule<BaseActivity, BaseFragment> {
    public PresenterModule(View view) {
        super(view);
    }

    public PresenterModule(BaseActivity activity) {
        super(activity);
    }

    public PresenterModule(BaseFragment fragment) {
        super(fragment);
    }
    
    @Provides @PresenterScope
    MainPresenter provideMainPresenter(MainPresenterMockImpl presenter) {
        return presenter;
    }
}
