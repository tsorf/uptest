package ru.frosteye.uptest.app.environment;

import android.app.Application;

import ru.frosteye.uptest.app.di.component.AppComponent;
import ru.frosteye.uptest.app.di.component.DaggerAppComponent;
import ru.frosteye.uptest.app.di.module.AppModule;


public class Uptest extends Application {

    private static AppComponent appComponent;

    public static AppComponent getAppComponent() {
        return appComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }
}
